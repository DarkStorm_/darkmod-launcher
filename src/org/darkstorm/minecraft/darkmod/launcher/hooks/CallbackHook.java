package org.darkstorm.minecraft.darkmod.launcher.hooks;

import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.*;
import org.darkstorm.minecraft.darkmod.launcher.ClassGenerator;
import org.jdom.Element;

public class CallbackHook extends Hook {
	private String className;
	private String interfaceName;
	private String method;
	private int position;
	private String callbackMethod;
	private String[] values;
	private int cancelTarget;
	private int callEventTarget;

	public CallbackHook(ClassGenerator classGenerator, Element element) {
		super(classGenerator, element);
	}

	public String getClassName() {
		return className;
	}

	@Override
	public String getInterfaceName() {
		return interfaceName;
	}

	public String getMethod() {
		return method;
	}

	public int getPosition() {
		return position;
	}

	public String getCallbackMethod() {
		return callbackMethod;
	}

	public int getCancelTarget() {
		return cancelTarget;
	}

	public int getCallEventTarget() {
		return callEventTarget;
	}

	public String[] getValues() {
		String[] values = new String[this.values.length];
		System.arraycopy(this.values, 0, values, 0, values.length);
		return values;
	}

	@Override
	protected void readElement(Element element) {
		className = element.getAttributeValue("class");
		interfaceName = element.getAttributeValue("interface");
		method = element.getAttributeValue("method");
		position = Integer.valueOf(element.getAttributeValue("position"));
		callbackMethod = element.getAttributeValue("callback");
		String valuesAttribute = element.getAttributeValue("values");
		if(valuesAttribute != null) {
			String[] values = valuesAttribute.split(",");
			if(values.length != 1 || !values[0].isEmpty())
				this.values = values;
		} else
			values = new String[0];
		String cancelAttribute = element.getAttributeValue("cancel");
		if(cancelAttribute != null)
			cancelTarget = Integer.parseInt(cancelAttribute);
		else
			cancelTarget = -1;
		String callAttribute = element.getAttributeValue("call");
		if(callAttribute != null)
			callEventTarget = Integer.parseInt(callAttribute);
		else
			callEventTarget = -1;
	}

	@Override
	public ClassGen generateInterface(ClassGen classGen) {
		if(classGen == null) {
			classGen = new ClassGen(interfaceName, "java.lang.Object", "Class.java", Constants.ACC_PUBLIC | Constants.ACC_FINAL, new String[0]);
		}
		String methodName = callbackMethod.split("\\(")[0];
		String methodSignature = callbackMethod.substring(methodName.length());
		String darkModClassName = "org.darkstorm.minecraft.darkmod.DarkMod";
		String modHandlerClassName = "org.darkstorm.minecraft.darkmod.mod.ModHandler";
		String eventManagerClassName = "org.darkstorm.tools.events.EventManager";
		String callbackEventClassName = "org.darkstorm.minecraft.darkmod.events.CallbackEvent";
		String eventClassName = "org.darkstorm.tools.events.Event";
		Type returnType = new ObjectType(callbackEventClassName);
		Type[] argTypes = Type.getArgumentTypes(methodSignature);

		for(Method method : classGen.getMethods())
			if(methodName.equals(method.getName()) && returnType.getSignature().equals(method.getSignature().split("\\)")[1]))
				return classGen;

		InstructionList list = new InstructionList();

		MethodGen method = new MethodGen(Constants.ACC_PUBLIC | Constants.ACC_STATIC, returnType, argTypes, null, methodName, classGen.getClassName(), list, classGen.getConstantPool());
		InstructionFactory factory = new InstructionFactory(classGen);

		list.append(factory.createInvoke(darkModClassName, "getInstance", new ObjectType(darkModClassName), new Type[0], Constants.INVOKESTATIC));
		list.append(factory.createInvoke(darkModClassName, "getModHandler", new ObjectType(modHandlerClassName), new Type[0], Constants.INVOKEVIRTUAL));
		InstructionHandle nullTarget = list.append(factory.createInvoke(modHandlerClassName, "getEventManager", new ObjectType(eventManagerClassName), new Type[0], Constants.INVOKEVIRTUAL));

		list.insert(nullTarget, new DUP());
		list.insert(nullTarget, new IFNONNULL(nullTarget));
		list.insert(nullTarget, new POP());
		list.insert(nullTarget, factory.createNew(new ObjectType(callbackEventClassName)));
		list.insert(nullTarget, new DUP());
		list.insert(nullTarget, factory.createConstant(methodName));
		list.insert(nullTarget, factory.createConstant(Integer.valueOf(argTypes.length)));
		list.insert(nullTarget, factory.createNewArray(Type.OBJECT, (short) 1));
		for(int i = 0; i < argTypes.length; i++) {
			list.insert(nullTarget, new DUP());
			list.insert(nullTarget, factory.createConstant(Integer.valueOf(i)));
			list.insert(nullTarget, InstructionFactory.createLoad(argTypes[i], i));
			if(argTypes[i] instanceof BasicType) {
				ObjectType autoboxType = (ObjectType) Type.getType(getAutoboxType((BasicType) argTypes[i]));
				list.insert(nullTarget, factory.createInvoke(autoboxType.getClassName(), "valueOf", autoboxType, new Type[] { argTypes[i] }, Constants.INVOKESTATIC));
			}
			list.insert(nullTarget, new AASTORE());
		}
		list.insert(nullTarget, factory.createInvoke(callbackEventClassName, "<init>", Type.VOID, new Type[] { Type.STRING, new ArrayType(Type.OBJECT, 1) }, Constants.INVOKESPECIAL));
		list.insert(nullTarget, new ARETURN());

		list.append(new ASTORE(argTypes.length));

		list.append(factory.createNew(new ObjectType(callbackEventClassName)));
		list.append(new DUP());

		list.append(factory.createConstant(methodName));

		list.append(factory.createConstant(Integer.valueOf(argTypes.length)));
		list.append(factory.createNewArray(Type.OBJECT, (short) 1));
		for(int i = 0; i < argTypes.length; i++) {
			list.append(new DUP());
			list.append(factory.createConstant(Integer.valueOf(i)));
			list.append(InstructionFactory.createLoad(argTypes[i], i));
			if(argTypes[i] instanceof BasicType) {
				ObjectType autoboxType = (ObjectType) Type.getType(getAutoboxType((BasicType) argTypes[i]));
				list.append(factory.createInvoke(autoboxType.getClassName(), "valueOf", autoboxType, new Type[] { argTypes[i] }, Constants.INVOKESTATIC));
			}
			list.append(new AASTORE());
		}

		list.append(factory.createInvoke(callbackEventClassName, "<init>", Type.VOID, new Type[] { Type.STRING, new ArrayType(Type.OBJECT, 1) }, Constants.INVOKESPECIAL));
		list.append(new ASTORE(argTypes.length + 1));

		list.append(new ALOAD(argTypes.length));
		list.append(new ALOAD(argTypes.length + 1));
		list.append(factory.createInvoke(eventManagerClassName, "sendEvent", Type.VOID, new Type[] { new ObjectType(eventClassName) }, Constants.INVOKEVIRTUAL));

		list.append(new ALOAD(argTypes.length + 1));
		list.append(InstructionFactory.createReturn(returnType));
		list.setPositions();
		method.setInstructionList(list);
		method.setMaxLocals();
		method.setMaxStack();
		classGen.addMethod(method.getMethod());
		return classGen;
	}

	private Class<?> getAutoboxType(BasicType type) {
		Class<?> autoboxType = null;
		switch(type.getType()) {
		case Constants.T_INT:
			autoboxType = Integer.class;
			break;
		case Constants.T_SHORT:
			autoboxType = Short.class;
			break;
		case Constants.T_LONG:
			autoboxType = Long.class;
			break;
		case Constants.T_BYTE:
			autoboxType = Byte.class;
			break;
		case Constants.T_DOUBLE:
			autoboxType = Double.class;
			break;
		case Constants.T_FLOAT:
			autoboxType = Float.class;
			break;
		case Constants.T_CHAR:
			autoboxType = Character.class;
			break;
		case Constants.T_BOOLEAN:
			autoboxType = Boolean.class;
			break;
		}
		return autoboxType;
	}
}