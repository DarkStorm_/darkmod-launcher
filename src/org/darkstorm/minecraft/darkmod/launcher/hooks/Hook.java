package org.darkstorm.minecraft.darkmod.launcher.hooks;

import org.darkstorm.minecraft.darkmod.launcher.ClassGenerator;
import org.jdom.Element;

import org.apache.bcel.generic.ClassGen;

public abstract class Hook {
	protected final ClassGenerator classGenerator;

	public Hook(ClassGenerator classGenerator, Element element) {
		this.classGenerator = classGenerator;
		readElement(element);
	}

	protected abstract void readElement(Element element);

	public abstract ClassGen generateInterface(ClassGen classGen);

	public abstract String getInterfaceName();
}
