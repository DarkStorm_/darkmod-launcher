package org.darkstorm.minecraft.darkmod.launcher.hooks;

import org.darkstorm.minecraft.darkmod.launcher.ClassGenerator;
import org.jdom.Element;

import org.apache.bcel.Constants;
import org.apache.bcel.generic.*;

public class MethodHook extends Hook {
	private String className, interfaceName, methodName, methodSignature,
			newMethodName, newMethodSignature;

	public MethodHook(ClassGenerator classGenerator, Element element) {
		super(classGenerator, element);
	}

	@Override
	protected void readElement(Element element) {
		className = element.getAttributeValue("class");
		methodName = element.getAttributeValue("method");
		methodSignature = element.getAttributeValue("signature");
		newMethodName = element.getAttributeValue("new_method");
		newMethodSignature = element.getAttributeValue("new_signature");
		element = element.getParentElement();
		interfaceName = element.getAttributeValue("interface");
	}

	public String getClassName() {
		return className;
	}

	@Override
	public String getInterfaceName() {
		return interfaceName;
	}

	public String getMethodName() {
		return methodName;
	}

	public String getMethodSignature() {
		return methodSignature;
	}

	public String getNewMethodName() {
		return newMethodName;
	}

	public String getNewMethodSignature() {
		return newMethodSignature;
	}

	@Override
	public ClassGen generateInterface(ClassGen classGen) {
		MethodGen method = new MethodGen(Constants.ACC_PUBLIC
				| Constants.ACC_ABSTRACT,
				Type.getReturnType(newMethodSignature),
				Type.getArgumentTypes(newMethodSignature), null, newMethodName,
				classGen.getClassName(), null, classGen.getConstantPool());
		classGen.addMethod(method.getMethod());
		return classGen;
	}
}
