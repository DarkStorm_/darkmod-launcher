package org.darkstorm.minecraft.darkmod.launcher.hooks;

import org.darkstorm.minecraft.darkmod.launcher.ClassGenerator;
import org.jdom.Element;

import org.apache.bcel.Constants;
import org.apache.bcel.generic.*;

public class SetterHook extends Hook {
	private String className, interfaceName, fieldName, fieldSignature,
			argumentType, setterName;
	private boolean isStatic;

	public SetterHook(ClassGenerator classGenerator, Element element) {
		super(classGenerator, element);
	}

	public String getClassName() {
		return className;
	}

	@Override
	public String getInterfaceName() {
		return interfaceName;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getFieldSignature() {
		return fieldSignature;
	}

	public boolean isStatic() {
		return isStatic;
	}

	public String getReturnType() {
		return argumentType;
	}

	public String getSetterName() {
		return setterName;
	}

	@Override
	protected void readElement(Element element) {
		className = element.getAttributeValue("class");
		fieldName = element.getAttributeValue("field");
		fieldSignature = element.getAttributeValue("signature");
		isStatic = Boolean.valueOf(element.getAttributeValue("static"));
		argumentType = element.getAttributeValue("argument");
		setterName = element.getAttributeValue("setter");
		element = element.getParentElement();
		interfaceName = element.getAttributeValue("interface");
	}

	@Override
	public ClassGen generateInterface(ClassGen classGen) {
		MethodGen method = new MethodGen(Constants.ACC_PUBLIC
				| Constants.ACC_ABSTRACT, Type.VOID,
				new Type[] { getType(argumentType) },
				new String[] { fieldName }, setterName,
				classGen.getClassName(), null, classGen.getConstantPool());
		classGen.addMethod(method.getMethod());
		return classGen;
	}

	private Type getType(String className) {
		if(className.endsWith("]"))
			return getArrayType(className);
		else if(className.equals("boolean"))
			return Type.BOOLEAN;
		else if(className.equals("byte"))
			return Type.BYTE;
		else if(className.equals("short"))
			return Type.SHORT;
		else if(className.equals("int"))
			return Type.INT;
		else if(className.equals("long"))
			return Type.LONG;
		else if(className.equals("float"))
			return Type.FLOAT;
		else if(className.equals("double"))
			return Type.DOUBLE;
		else if(className.equals("char"))
			return Type.CHAR;
		return new ObjectType(className);
	}

	private ArrayType getArrayType(String className) {
		String baseClassName = "";
		int dimensions = 0;
		for(char character : className.toCharArray())
			if(character == '[')
				dimensions++;
			else if(character != ']')
				baseClassName += character;
		return new ArrayType(baseClassName, dimensions);
	}
}
