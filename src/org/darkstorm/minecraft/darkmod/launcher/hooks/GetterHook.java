package org.darkstorm.minecraft.darkmod.launcher.hooks;

import org.darkstorm.minecraft.darkmod.launcher.ClassGenerator;
import org.jdom.Element;

import org.apache.bcel.Constants;
import org.apache.bcel.generic.*;

public class GetterHook extends Hook {
	private String className, interfaceName, fieldName, fieldSignature,
			returnType, getterName;
	private boolean isStatic;

	public GetterHook(ClassGenerator classGenerator, Element element) {
		super(classGenerator, element);
	}

	public String getClassName() {
		return className;
	}

	@Override
	public String getInterfaceName() {
		return interfaceName;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getFieldSignature() {
		return fieldSignature;
	}

	public String getReturnType() {
		return returnType;
	}

	public String getGetterName() {
		return getterName;
	}

	public boolean isStatic() {
		return isStatic;
	}

	@Override
	protected void readElement(Element element) {
		className = element.getAttributeValue("class");
		fieldName = element.getAttributeValue("field");
		fieldSignature = element.getAttributeValue("signature");
		isStatic = Boolean.valueOf(element.getAttributeValue("static"));
		returnType = element.getAttributeValue("return");
		getterName = element.getAttributeValue("getter");
		element = element.getParentElement();
		interfaceName = element.getAttributeValue("interface");
	}

	@Override
	public ClassGen generateInterface(ClassGen classGen) {
		MethodGen method = new MethodGen(Constants.ACC_PUBLIC
				| Constants.ACC_ABSTRACT, getType(returnType), Type.NO_ARGS,
				new String[0], getterName, classGen.getClassName(), null,
				classGen.getConstantPool());
		classGen.addMethod(method.getMethod());
		return classGen;
	}

	private Type getType(String className) {
		if(className.endsWith("]"))
			return getArrayType(className);
		else if(className.equals("boolean"))
			return Type.BOOLEAN;
		else if(className.equals("byte"))
			return Type.BYTE;
		else if(className.equals("short"))
			return Type.SHORT;
		else if(className.equals("int"))
			return Type.INT;
		else if(className.equals("long"))
			return Type.LONG;
		else if(className.equals("float"))
			return Type.FLOAT;
		else if(className.equals("double"))
			return Type.DOUBLE;
		else if(className.equals("char"))
			return Type.CHAR;
		return new ObjectType(className);
	}

	private ArrayType getArrayType(String className) {
		String baseClassName = "";
		int dimensions = 0;
		for(char character : className.toCharArray())
			if(character == '[')
				dimensions++;
			else if(character != ']')
				baseClassName += character;
		Type baseClassType = getType(baseClassName);
		return new ArrayType(baseClassType, dimensions);
	}
}
