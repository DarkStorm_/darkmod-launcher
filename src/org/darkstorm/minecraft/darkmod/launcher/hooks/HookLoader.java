package org.darkstorm.minecraft.darkmod.launcher.hooks;

import java.io.*;
import java.net.URL;
import java.util.*;

import javax.swing.JOptionPane;

import org.darkstorm.minecraft.darkmod.launcher.*;
import org.jdom.*;
import org.jdom.input.SAXBuilder;
import org.jdom.output.*;

public class HookLoader {
	private ClassGenerator classGenerator;

	private String version;

	public HookLoader(ClassGenerator classGenerator) {
		this.classGenerator = classGenerator;
	}

	public Hook[] loadHooks() {
		DarkModLauncher launcher = DarkModLauncher.getInstance();
		launcher.setStatus("Loading hooks...");
		Document document = loadXML();
		outputXML(document);
		launcher.setProgress(25);
		launcher.setStatus("Parsing hook data...");
		return parseHooks(document);
	}

	private Document loadXML() {
		Exception exception1 = null, exception2 = null;
		try {
			if(Tools.isRunningFromJar())
				return new SAXBuilder().build(new URL("http://darkstorm652.webs.com/darkmod/Hooks (" + Tools.getMinecraftBuild() + ").xml"));
		} catch(Exception exception) {
			exception1 = exception;
		}
		try {
			return new SAXBuilder().build(new File("Hooks.xml"));
		} catch(Exception exception) {
			exception2 = exception;
		}
		try {
			return new SAXBuilder().build(getClass().getResource("/Hooks.xml"));
		} catch(Exception exception) {
			if(exception1 != null)
				exception1.printStackTrace();
			if(exception2 != null)
				exception2.printStackTrace();
			exception.printStackTrace();
			exit("No hook data found. (Hooks.xml)");
		}
		return null;
	}

	private void outputXML(Document document) {
		try {
			XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
			File hooksFile = new File("./Hooks.xml");
			File hooksDir = hooksFile.getParentFile();
			if(!hooksDir.exists())
				hooksFile.mkdirs();
			FileOutputStream outputStream = new FileOutputStream(hooksFile);
			outputter.output(document, outputStream);
		} catch(Exception exception) {}
	}

	@SuppressWarnings("unchecked")
	private Hook[] parseHooks(Document document) {
		List<Hook> hooks = new ArrayList<Hook>();
		Element rootElement = document.getRootElement();
		version = rootElement.getAttributeValue("version");
		List<Element> hookElements = rootElement.getChildren();
		DarkModLauncher launcher = DarkModLauncher.getInstance();
		int progress = 0;
		for(Element element : hookElements) {
			String type = element.getName();
			if(type.equals("interface")) {
				hooks.add(new InterfaceHook(classGenerator, element));
				for(Element interfaceElement : (List<Element>) element.getChildren()) {
					type = interfaceElement.getName();
					if(type.equals("getter"))
						hooks.add(new GetterHook(classGenerator, interfaceElement));
					else if(type.equals("setter"))
						hooks.add(new SetterHook(classGenerator, interfaceElement));
					else if(type.equals("method"))
						hooks.add(new MethodHook(classGenerator, interfaceElement));
					else
						exit("Error loading hooks: invalid type");
				}
			} else if(type.equals("callback"))
				hooks.add(new CallbackHook(classGenerator, element));
			else if(type.equals("bytecode"))
				hooks.add(new BytecodeHook(classGenerator, element));
			else
				exit("Error loading hooks: invalid type");
			progress++;
			launcher.setProgress(25 + (int) (25D * ((double) progress / (double) hookElements.size())));
		}
		return hooks.toArray(new Hook[hooks.size()]);
	}

	private void exit(String message) {
		DarkModLauncher.getInstance().setStatus("Error");
		DarkModLauncher.getInstance().setProgress(100);
		JOptionPane.showMessageDialog(DarkModLauncher.getInstance(), message, "Error", JOptionPane.ERROR_MESSAGE);
		System.exit(-1);
	}

	public ClassGenerator getClassGenerator() {
		return classGenerator;
	}

	public String getVersion() {
		return version;
	}
}