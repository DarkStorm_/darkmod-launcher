package org.darkstorm.minecraft.darkmod.launcher.hooks;

import org.darkstorm.minecraft.darkmod.launcher.ClassGenerator;
import org.jdom.Element;

import org.apache.bcel.generic.ClassGen;

public class BytecodeHook extends Hook {
	private String className;
	private String methodName;
	private String methodSignature;
	private int position;
	private Element[] instructions;

	public BytecodeHook(ClassGenerator classGenerator, Element element) {
		super(classGenerator, element);
	}

	public String getClassName() {
		return className;
	}

	public String getMethodName() {
		return methodName;
	}

	public String getMethodSignature() {
		return methodSignature;
	}

	public int getPosition() {
		return position;
	}

	public Element[] getInstructions() {
		return instructions;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void readElement(Element element) {
		className = element.getAttributeValue("class");
		methodName = element.getAttributeValue("method");
		methodSignature = element.getAttributeValue("signature");
		position = Integer.valueOf(element.getAttributeValue("position"));
		instructions = (Element[]) element.getChildren("instruction").toArray(
				new Element[0]);
	}

	@Override
	public String getInterfaceName() {
		return "bytecode";
	}

	@Override
	public ClassGen generateInterface(ClassGen classGen) {
		return null;
	}
}
