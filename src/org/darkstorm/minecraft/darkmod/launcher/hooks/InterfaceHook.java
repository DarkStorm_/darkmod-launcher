package org.darkstorm.minecraft.darkmod.launcher.hooks;

import org.darkstorm.minecraft.darkmod.launcher.ClassGenerator;
import org.jdom.Element;

import org.apache.bcel.Constants;
import org.apache.bcel.generic.ClassGen;

public class InterfaceHook extends Hook {
	private String className;
	private String interfaceName;
	private String[] interfaces;

	public InterfaceHook(ClassGenerator classGenerator, Element element) {
		super(classGenerator, element);
	}

	public String getClassName() {
		return className;
	}

	@Override
	public String getInterfaceName() {
		return interfaceName;
	}

	public String[] getInterfaces() {
		return interfaces;
	}

	@Override
	protected void readElement(Element element) {
		className = element.getAttributeValue("class");
		interfaceName = element.getAttributeValue("interface");
		String interfaceNames = element.getAttributeValue("interfaces");
		if(interfaceNames != null)
			interfaces = interfaceNames.split(",");
		else
			interfaces = new String[0];
	}

	@Override
	public ClassGen generateInterface(ClassGen classGen) {
		classGen = new ClassGen(interfaceName, "java.lang.Object",
				"Class.java", Constants.ACC_PUBLIC | Constants.ACC_INTERFACE
						| Constants.ACC_ABSTRACT, interfaces);
		return classGen;
	}
}
