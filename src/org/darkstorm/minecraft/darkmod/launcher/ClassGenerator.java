package org.darkstorm.minecraft.darkmod.launcher;

import java.util.*;
import java.util.jar.*;

import java.io.*;

import org.darkstorm.minecraft.darkmod.launcher.hooks.*;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.generic.ClassGen;

public class ClassGenerator {
	private Hook[] hooks;

	public ClassGenerator() {
		HookLoader loader = new HookLoader(this);
		hooks = loader.loadHooks();
	}

	public CustomClassLoader generateClasses() {
		DarkModLauncher launcher = DarkModLauncher.getInstance();
		launcher.setStatus("Generating API...");
		launcher.setProgress(50);
		int hooked = 0;
		Map<String, ClassGen> classes = new HashMap<String, ClassGen>();
		for(Hook hook : hooks) {
			if(hook instanceof InterfaceHook) {
				classes.put(hook.getInterfaceName(),
						hook.generateInterface(null));
				launcher.setProgress((int) (25D * ((double) ++hooked / (double) hooks.length)));
			}
		}
		for(Hook hook : hooks) {
			if(!(hook instanceof InterfaceHook)) {
				classes.put(hook.getInterfaceName(),
						hook.generateInterface(classes.get(hook
								.getInterfaceName())));
				launcher.setProgress(50 + (int) (25D * ((double) ++hooked / (double) hooks.length)));
			}
		}
		dumpJar(classes.values());
		HashMap<String, byte[]> outputClasses = new HashMap<String, byte[]>();
		for(ClassGen classGen : classes.values()) {
			if(classGen == null)
				continue;
			JavaClass javaClass = classGen.getJavaClass();
			outputClasses.put(classGen.getClassName(), javaClass.getBytes());
		}
		return new CustomClassLoader(outputClasses);
	}

	private void dumpJar(Collection<ClassGen> classes) {
		try {
			File file = new File("api/api.jar");
			FileOutputStream stream = new FileOutputStream(file);
			JarOutputStream out = new JarOutputStream(stream);
			for(ClassGen classGen : classes) {
				if(classGen == null)
					continue;
				JarEntry jarEntry = new JarEntry(classGen.getClassName()
						.replace('.', '/') + ".class");
				out.putNextEntry(jarEntry);
				out.write(classGen.getJavaClass().getBytes());
			}
			out.close();
			stream.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
