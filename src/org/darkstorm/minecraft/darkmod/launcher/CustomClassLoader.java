package org.darkstorm.minecraft.darkmod.launcher;

import java.util.*;

import java.io.*;
import java.net.*;
import java.security.ProtectionDomain;

public class CustomClassLoader extends URLClassLoader {
	private Map<String, byte[]> classes;
	private Map<URL, Map<String, byte[]>> resources;

	public CustomClassLoader(Map<String, byte[]> classes) {
		super(new URL[0], CustomClassLoader.class.getClassLoader());
		this.classes = classes;
		resources = new HashMap<URL, Map<String, byte[]>>();
	}

	public void addClass(String name, byte[] data) {
		synchronized(classes) {
			classes.put(name, data);
		}
	}

	public void addResource(URL source, String name, byte[] data) {
		synchronized(resources) {
			Map<String, byte[]> sourceResources = resources.get(source);
			if(sourceResources == null) {
				sourceResources = new HashMap<String, byte[]>();
				resources.put(source, sourceResources);
			}
			sourceResources.put(name, data);
		}
	}

	@Override
	public URL findResource(String name) {
		synchronized(resources) {
			for(URL source : resources.keySet()) {
				Map<String, byte[]> sourceResources = resources.get(source);
				if(sourceResources.containsKey(name)) {
					final byte[] data = sourceResources.get(name);
					try {
						URL url = new URL(source, name, new URLStreamHandler() {

							@Override
							protected URLConnection openConnection(URL u)
									throws IOException {
								URLConnection connection = new URLConnection(u) {
									private ByteArrayInputStream in;

									@Override
									public void connect() throws IOException {
										in = new ByteArrayInputStream(data);
									}

									@Override
									public InputStream getInputStream()
											throws IOException {
										return in;
									}
								};
								connection.connect();
								return connection;
							}
						});
						return url;
					} catch(Exception exception) {
						exception.printStackTrace();
					}
				}
			}
		}
		return super.findResource(name);
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		if(!classes.containsKey(name))
			return super.findClass(name);
		byte[] data;
		synchronized(classes) {
			data = classes.get(name);
		}
		return defineClass(name, data, 0, data.length, (ProtectionDomain) null);
	}

	@Override
	public void addURL(URL url) {
		super.addURL(url);
	}
}
