package org.darkstorm.minecraft.darkmod.launcher;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.jar.*;
import java.util.zip.*;

import javax.imageio.ImageIO;
import javax.swing.*;

@SuppressWarnings("serial")
public final class DarkModLauncher extends JDialog {
	private static DarkModLauncher instance;

	private JLabel status;
	private JProgressBar bar;

	public DarkModLauncher() throws Exception {
		this("../DarkMod");
	}

	public DarkModLauncher(String path) throws Exception {
		if(instance != null)
			throw new IllegalStateException("Instance already exists");
		instance = this;

		initComponents();

		ClassGenerator generator = new ClassGenerator();
		CustomClassLoader loader = generator.generateClasses();
		setStatus("Loading classes...");
		setProgress(75);
		if(!Tools.isRunningFromJar()) {
			loader.addURL(new File(path + "/bin").getAbsoluteFile().toURI().toURL());
			for(File file : getAllFilesRecursively(new File(path, "lib"))) {
				if(file.getName().toLowerCase().endsWith(".jar")) {
					loader.addURL(file.toURI().toURL());
					System.out.println("Found DarkMod lib: " + file.getName());
				}
			}
		} else {
			// Mother of all hackish solutions: jar inside a jar
			URL darkmod = getClass().getResource("/DarkMod.jar");
			JarInputStream in = new JarInputStream(darkmod.openStream());
			JarEntry entry;
			while((entry = in.getNextJarEntry()) != null) {
				byte[] data = readAll(in);
				if(entry.getName().endsWith(".class")) {
					String entryName = entry.getName();
					entryName = entryName.replace('/', '.');
					entryName = entryName.substring(0, entry.getName().length() - ".class".length());
					loader.addClass(entryName, data);
				} else
					loader.addResource(darkmod, entry.getName(), data);
				in.closeEntry();
			}
		}
		File nativeDir = new File(Tools.getMinecraftDirectory(), "natives-temp");
		nativeDir.deleteOnExit();
		for(File file : getAllFilesRecursively(new File(Tools.getMinecraftDirectory(), "libraries"))) {
			if(!file.getName().toLowerCase().endsWith(".jar"))
				continue;
			if(file.getName().toLowerCase().contains("natives")) {
				ZipFile zip = new ZipFile(file);
				try {
					Enumeration<? extends ZipEntry> entries = zip.entries();

					while(entries.hasMoreElements()) {
						ZipEntry entry = entries.nextElement();

						File targetFile = new File(nativeDir, entry.getName());
						if(targetFile.getParentFile() != null)
							targetFile.getParentFile().mkdirs();

						if(!entry.isDirectory()) {
							BufferedInputStream inputStream = new BufferedInputStream(zip.getInputStream(entry));

							byte[] buffer = new byte[2048];
							FileOutputStream outputStream = new FileOutputStream(targetFile);
							BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
							try {
								int length;
								while((length = inputStream.read(buffer, 0, buffer.length)) != -1)
									bufferedOutputStream.write(buffer, 0, length);
							} finally {
								try {
									bufferedOutputStream.close();
								} catch(IOException exception) {}
								try {
									outputStream.close();
								} catch(IOException exception) {}
								try {
									inputStream.close();
								} catch(IOException exception) {}
							}
						}
					}
				} finally {
					zip.close();
				}
				System.out.println("Extracted natives: " + file.getName());
			} else {
				loader.addURL(file.toURI().toURL());
				System.out.println("Found lib: " + file.getName());
			}
		}
		System.setProperty("org.lwjgl.librarypath", nativeDir.getAbsolutePath());
		System.setProperty("net.java.games.input.librarypath", nativeDir.getAbsolutePath());
		Class<?> darkMod;
		try {
			darkMod = loader.loadClass("org.darkstorm.minecraft.darkmod.DarkMod");
		} catch(Exception exception) {
			exception.printStackTrace();
			JOptionPane.showMessageDialog(this, "Unable to load DarkMod.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(-1);
			return;
		}
		setStatus("Done.");
		setProgress(100);
		setVisible(false);
		darkMod.newInstance();
	}

	private File[] getAllFilesRecursively(File dir) {
		List<File> files = new ArrayList<>();
		addFiles(files, dir);
		return files.toArray(new File[files.size()]);
	}

	private void addFiles(List<File> files, File dir) {
		for(File file : dir.listFiles())
			if(file.isDirectory())
				addFiles(files, file);
			else
				files.add(file);
	}

	private byte[] readAll(InputStream in) throws IOException {
		ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int bytesRead;
		while((bytesRead = in.read(buffer)) != -1)
			byteArrayOut.write(buffer, 0, bytesRead);
		byteArrayOut.close();
		return byteArrayOut.toByteArray();
	}

	/*private URL[] locateLWJGL() {
		File workingDir = Tools.getMinecraftDirectory();
		String path = workingDir.getAbsolutePath() + "/bin/";
		File dir = new File(path);
		try {
			String[] jarNames = new String[] { "lwjgl.jar", "jinput.jar",
					"lwjgl_util.jar" };
			URL[] urls = new URL[jarNames.length];
			for(int i = 0; i < jarNames.length; i++) {
				String jarName = jarNames[i];
				urls[i] = new File(dir, jarName).toURI().toURL();
			}
			System.setProperty("org.lwjgl.librarypath", path + "/natives");
			System.setProperty("net.java.games.input.librarypath", path
					+ "/natives");
			return urls;
		} catch(Exception exception) {
			exception.printStackTrace();
			JOptionPane.showMessageDialog(this, "Could not locate LWJGL.",
					"Error", JOptionPane.ERROR_MESSAGE);
			System.exit(-1);
		}
		return null;
	}*/

	private void initComponents() {
		SplashScreen screen = SplashScreen.getSplashScreen();

		setUndecorated(true);
		setTitle("DarkMod");
		BufferedImage background;
		try {
			background = ImageIO.read(getClass().getResourceAsStream("/DarkModLogo.png"));
		} catch(Exception exception) {
			exception.printStackTrace();
			background = new BufferedImage(500, 300, BufferedImage.TYPE_INT_ARGB);
			Graphics g = background.getGraphics();
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, 500, 300);
			g.setColor(Color.WHITE);
			g.setFont(g.getFont().deriveFont(Font.BOLD, 24F));
			String message = "DarkMod";
			g.drawString(message, (500 / 2) - (g.getFontMetrics().stringWidth(message) / 2), (300 / 2) - g.getFontMetrics().getHeight());
			g.setFont(g.getFont().deriveFont(Font.PLAIN, 10F));
			message = "Logo not found";
			g.drawString(message, (500 / 2) - (g.getFontMetrics().stringWidth(message) / 2), 300 / 2);
		}
		if(screen == null || !screen.isVisible()) {
			setSize(background.getWidth(), background.getHeight());
			setPreferredSize(getSize());
			setLocationRelativeTo(null);
		} else
			setBounds(screen.getBounds());

		status = new JLabel("Loading...");
		status.setForeground(Color.WHITE);
		bar = new JProgressBar();

		GridBagLayout layout = new GridBagLayout();
		setLayout(layout);
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.CENTER;

		layout.columnWidths = new int[] { 0, 0 };
		layout.rowHeights = new int[] { 0, 0, 0, 0 };
		layout.columnWeights = new double[] { 1.0, 1.0E-4 };
		layout.rowWeights = new double[] { 1.0, 0.0, 0.0, 1.0E-4 };

		constraints.gridy++;
		add(status, constraints);
		constraints.gridy++;
		add(bar, constraints);

		setContentPane(wrapInBackgroundImage((JComponent) getContentPane(), new ImageIcon(background)));

		setVisible(true);

		if(screen != null && screen.isVisible())
			screen.close();
	}

	private JPanel wrapInBackgroundImage(JComponent component, Icon backgroundIcon) {
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.NORTHWEST;

		component.setOpaque(false);

		JPanel backgroundPanel = new JPanel(new GridBagLayout());
		backgroundPanel.add(component, constraints);
		JLabel backgroundImage = new JLabel(backgroundIcon);
		backgroundImage.setPreferredSize(new Dimension(1, 1));
		backgroundImage.setMinimumSize(new Dimension(1, 1));
		backgroundImage.setVerticalAlignment(JLabel.TOP);
		backgroundImage.setHorizontalAlignment(JLabel.LEADING);
		backgroundPanel.add(backgroundImage, constraints);

		return backgroundPanel;
	}

	public static void main(String[] args) throws Exception {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception exception) {}
		if(args.length > 0)
			new DarkModLauncher(args[0]);
		else
			new DarkModLauncher();
	}

	public void setStatus(String status) {
		this.status.setText(status);
	}

	public void setProgress(int progress) {
		bar.setValue(progress);
	}

	public static DarkModLauncher getInstance() {
		return instance;
	}
}